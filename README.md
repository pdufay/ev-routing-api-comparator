# EV-routing-API-comparator

Web tool to compare electric vehicle routing APIs from Tomtom, Here and BeNomad
Developped in july 2022 by Jonathan Hayot

API docs:
- https://bemap-ev-move-preprod.benomad.com/bgis/documentation/index.html#page-evreachablearea-service.md
- https://developer.here.com/documentation/routing-api/dev_guide/topics/use-cases/ev-routing.html#charging-time-calculation
- https://developer.tomtom.com/routing-api/api-explorer

BeNomad credentials:
- Login:             Mapping_factory_MI
- Password:      R2TJwKYqsXCez23CjfaI
- Expiration date: 30/09/2022
