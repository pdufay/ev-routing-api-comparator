
var map = new maplibregl.Map({
    container: 'map',
    style:
        'https://api.maptiler.com/maps/streets/style.json?key=USyQAZpTvHb4x7QZk5xD',
    zoom: 4,
    center: [1, 47],
    doubleClickZoom: false,
});

var markerStart = new maplibregl.Marker({
    draggable: true,
    color: '#00CC00'
})
    .setLngLat([0, 47])
    .setPopup(new maplibregl.Popup({ closeButton: false }).setHTML("<h2>Départ!!</h2>"))
    .addTo(map);
var markerEnd = new maplibregl.Marker({
    draggable: true,
    color: '#FF0000'
})
    .setLngLat([2, 47])
    .setPopup(new maplibregl.Popup({ closeButton: false }).setHTML("<h2>Arrivée!!</h2>"))
    .addTo(map);

var color1 = "#000";
var color2 = "#C0C0C0";
var size1 = 6;
var size2 = 5;

map.on('load', function () {
    var layers = [];
    var markers = [];

    var popup = [];
    var endPopup = [];
    var startPopup = [];
    var latlng = [];
    //
    //
    //      Function for TOMTOM api
    //
    //
    var routesTomtom = async (data, currentCharge, maxCharge, consumptionPer100km, color, id, size) => {
        var lngLat1 = markerStart.getLngLat();
        var lngLat2 = markerEnd.getLngLat();

        var apiURL = "https://api.tomtom.com/routing/1/calculateLongDistanceEVRoute/" + lngLat1.lat + "%2C" + lngLat1.lng + "%3A" + lngLat2.lat + "%2C" + lngLat2.lng + "/json?vehicleEngineType=electric&constantSpeedConsumptionInkWhPerHundredkm=50.0%2C" + consumptionPer100km.fifty + "%3A100.0%2C" + consumptionPer100km.hundred + "&currentChargeInkWh=" + currentCharge + "&maxChargeInkWh=" + maxCharge + "&minChargeAtDestinationInkWh=5.2&minChargeAtChargingStopsInkWh=1.5&key=ROVLLJbGICuZ4R5YY4ZzuBfLPhVGQpaG"
        var lnglat = $.ajax({
            headers: {
                'accept': '*/*',
                'content-type': 'application/json'
            },
            data: JSON.stringify(data),
            type: 'POST',
            url: apiURL,
            dataType: 'json',
            error: function (xhr) { console.log(xhr) }
        })
            .then((res) => { console.log(res); return res });

        var printTravelTomtom = async () => {
            const longlat = await lnglat;
            const lnglatfetch = longlat.routes[0].legs;
            var element = [];
            var k = 0;

            if (lnglatfetch.length > 1) {
                for (let i = 0; i < lnglatfetch.length - 1; i++) {
                    let name = lnglatfetch[i].summary.chargingInformationAtEndOfLeg.chargingParkName
                    let time = secondsToHms(lnglatfetch[i].summary.travelTimeInSeconds);
                    let lengthWay = lnglatfetch[i].summary.lengthInMeters / 1000;
                    let lengthh = lnglatfetch[i].points.length;
                    let chargeRemaining = Math.round(lnglatfetch[i].summary.remainingChargeAtArrivalInkWh / maxCharge * 100);
                    let chargeTarget = Math.round(lnglatfetch[i].summary.chargingInformationAtEndOfLeg.targetChargeInkWh / maxCharge * 100);
                    let chargeTime = secondsToHms(lnglatfetch[i].summary.chargingInformationAtEndOfLeg.chargingTimeInSeconds);
                    let stage = i + 1;
                    var letLatlng = [lnglatfetch[i].points[lengthh - 1].longitude, lnglatfetch[i].points[lengthh - 1].latitude];
                    let letPopup = '<h2>Stage ' + stage + id[1] + '</h2><strong>Charge station name: </strong>' + name + '<br/><strong>Travel time: </strong>' + time + '<br/><strong>Travel length: </strong>' + lengthWay + ' km<br/><strong>Charge: </strong>' + chargeRemaining + '% -> ' + chargeTarget + '%<br/><strong>Charge time: </strong>' + chargeTime + '</br>';
                    popup.push(letPopup);
                    latlng.push(letLatlng);
                    var marker = new maplibregl.Marker({
                        color: color
                    })
                        .setLngLat(letLatlng)
                        .addTo(map);
                    var is = false;
                    for (let i = 0; i < latlng.length - 1; i++) {
                        if (latlng[i][0] == letLatlng[0] && latlng[i][1] == letLatlng[1]) {
                            let oldPopup = popup[i];
                            marker.setPopup(new maplibregl.Popup({ closeButton: false }).setHTML(oldPopup + letPopup));
                            var is = true;
                        }
                    }

                    if (is == false) {
                        marker.setPopup(new maplibregl.Popup({ closeButton: false }).setHTML(letPopup));
                    }
                    markers.push(marker);
                }
            }

            let lng = lnglatfetch.length - 1;
            let timeEnd = secondsToHms(lnglatfetch[lng].summary.travelTimeInSeconds);
            let lengthWayEnd = lnglatfetch[lng].summary.lengthInMeters / 1000;
            let totalLength = longlat.routes[0].summary.lengthInMeters / 1000;
            let totalChargingTime = secondsToHms(longlat.routes[0].summary.totalChargingTimeInSeconds);
            let totalTime = secondsToHms(longlat.routes[0].summary.travelTimeInSeconds);
            let remainingCharge = Math.round(longlat.routes[0].summary.remainingChargeAtArrivalInkWh / maxCharge * 100);
            let lastStage = longlat.routes[0].legs.length;
            let startCharge = currentCharge / maxCharge * 100;
            var endPopupTom = '<h2>STAGE ' + lastStage + id[1] + '</h2><strong>Travel time: </strong>' + timeEnd + '<br/><strong>Travel length: </strong>' + lengthWayEnd + ' km<br/><h3>TOTAL ROUTES</h3><strong>Travel time: </strong>' + totalTime + '<br/><strong>Charging time: </strong>' + totalChargingTime + '<br/><strong>Travel length: </strong>' + totalLength + 'km<br/><strong>Remaining charge: </strong>' + remainingCharge + '%';
            var startPopupTom = '<h3>START ' + id[1] + '</h3><strong>Max charge: </strong>' + maxCharge + 'kWh</br><strong>Current charge: </strong>' + startCharge + '%</br>';
            endPopup.push(endPopupTom);
            startPopup.push(startPopupTom);
            var is = false;
            var endPopupOld = [];
            var startPopupOld = [];
            for (let k = 0; k < startPopup.length - 1; k++) {
                endPopupOld += endPopup[k];
                startPopupOld += startPopup[k];
                var is = true;
            }
            if (is == false) {
                markerEnd.setPopup(new maplibregl.Popup({ closeButton: false }).setHTML(endPopupTom));
                markerStart.setPopup(new maplibregl.Popup({ closeButton: false }).setHTML(startPopupTom));
            }
            else {
                markerEnd.setPopup(new maplibregl.Popup({ closeButton: false }).setHTML(endPopupOld + endPopupTom));
                markerStart.setPopup(new maplibregl.Popup({ closeButton: false }).setHTML(startPopupOld + startPopupTom));
            }

            for (let j = 0; j < lnglatfetch.length; j++) {
                for (let i = 0; i < lnglatfetch[j].points.length; i++) {
                    element[k] = [lnglatfetch[j].points[i].longitude, lnglatfetch[j].points[i].latitude];
                    k++;
                }
            }

            map.addSource('route' + id[0], {
                'type': 'geojson',
                'data': {
                    'type': 'Feature',
                    'properties': {},
                    'geometry': {
                        'type': 'LineString',
                        'coordinates': element
                    }
                }
            });
            map.addLayer({
                'id': 'route' + id[0],
                'type': 'line',
                'source': 'route' + id[0],
                'layout': {
                    'line-join': 'round',
                    'line-cap': 'round'
                },
                'paint': {
                    'line-color': color,
                    'line-width': size
                }
            });
            layers.push('route' + id[0]);
        };
        printTravelTomtom();
    };
    //
    //
    //      Functions for HERE api
    //
    //
    var generateToken = () => {
        // #1 Initialize OAuth with your HERE OAuth credentials from the credentials file that you downloaded above
        const oauth = OAuth({
            consumer: {
                key: '7_A6rJs33ZAiu1-rNZ5GhA', //Access key
                secret: '2SaDoD6oK2GuBVeABW2cT-lBFuBEGdN3UyvIWAl7hg1ueIEnKy-klV2jm5tmeP0qlfQexRRSL2XYoZp4fIQyrQ', //Secret key
            },
            signature_method: 'HMAC-SHA256',
            hash_function(base_string, key) {
                return CryptoJS.HmacSHA256(base_string, key).toString(CryptoJS.enc.Base64);
            }
        });
        // #2 Building the request object.
        const request_data = {
            url: 'https://account.api.here.com/oauth2/token',
            method: 'POST',
            data: { grant_type: 'client_credentials' },
        };
        // #3 Sending the request to get the access token
        var settings = {
            "url": request_data.url,
            "method": request_data.method,
            "headers": oauth.toHeader(oauth.authorize(request_data)),
            "data": request_data.data
        };

        var bearer = $.ajax(settings).then((res) => { return res });
        return bearer;
    }
    var routesHere = async (startCharge, maxCharge) => {
        const bearerToken = await generateToken();
        var lngLat1 = markerStart.getLngLat();
        var lngLat2 = markerEnd.getLngLat();
        var routesPromise = [];
        var HereURL = [
            "https://router.hereapi.com/v8/routes?departureTime=any&origin=" + lngLat1.lat + "," + lngLat1.lng + "&ev[connectorTypes]=iec62196Type2Combo&transportMode=car&destination=" + lngLat2.lat + "," + lngLat2.lng + "&return=polyline,summary&ev[freeFlowSpeedTable]=0,0.239,27,0.239,45,0.259,60,0.196,75,0.207,90,0.238,100,0.26,110,0.296,120,0.337,130,0.351,250,0.351&ev[trafficSpeedTable]=0,0.349,27,0.319,45,0.329,60,0.266,75,0.287,90,0.318,100,0.33,110,0.335,120,0.35,130,0.36,250,0.36&ev[auxiliaryConsumption]=1.8&ev[ascent]=9&ev[descent]=4.3&ev[makeReachable]=true&ev[initialCharge]=" + startCharge + "&ev[maxCharge]=" + maxCharge + "&ev[chargingCurve]=" + ((maxCharge - 8) * 0) + ",239," + ((maxCharge - 8) * 0, 4) + ",199," + ((maxCharge - 8) * 0.7) + ",167," + ((maxCharge - 8) * 0.75) + ",130," + ((maxCharge - 8) * 0.8) + ",111," + ((maxCharge - 8) * 0.85) + ",83," + ((maxCharge - 8) * 0.9) + ",55," + ((maxCharge - 8) * 0.95) + ",33," + ((maxCharge - 8) * 0.975) + ",17," + (maxCharge - 8) + ",1&ev[maxChargeAfterChargingStation]=" + (maxCharge - 8),
            "https://router.hereapi.com/v8/routes?departureTime=any&origin=" + lngLat1.lat + "," + lngLat1.lng + "&ev[connectorTypes]=iec62196Type2Combo&transportMode=car&destination=" + lngLat2.lat + "," + lngLat2.lng + "&return=summary&ev[freeFlowSpeedTable]=0,0.239,27,0.239,45,0.259,60,0.196,75,0.207,90,0.238,100,0.26,110,0.296,120,0.337,130,0.351,250,0.351&ev[trafficSpeedTable]=0,0.349,27,0.319,45,0.329,60,0.266,75,0.287,90,0.318,100,0.33,110,0.335,120,0.35,130,0.36,250,0.36&ev[auxiliaryConsumption]=1.8&ev[ascent]=9&ev[descent]=4.3&ev[makeReachable]=true&ev[initialCharge]=" + startCharge + "&ev[maxCharge]=" + maxCharge + "&ev[chargingCurve]=" + ((maxCharge - 8) * 0) + ",239," + ((maxCharge - 8) * 0, 4) + ",199," + ((maxCharge - 8) * 0.7) + ",167," + ((maxCharge - 8) * 0.75) + ",130," + ((maxCharge - 8) * 0.8) + ",111," + ((maxCharge - 8) * 0.85) + ",83," + ((maxCharge - 8) * 0.9) + ",55," + ((maxCharge - 8) * 0.95) + ",33," + ((maxCharge - 8) * 0.975) + ",17," + (maxCharge - 8) + ",1&ev[maxChargeAfterChargingStation]=" + (maxCharge - 8)
        ];
        for (let i = 0; i < HereURL.length; i++) {
            var settings = {
                "url": HereURL[i],
                "method": "GET",
                "timeout": 0,
                "headers": {
                    "Authorization": "Bearer " + bearerToken.access_token
                }
            };
            routesPromise.push($.ajax(settings)
                .then(res => { return res.routes[0].sections })
            )
        }
        var printTravelHere = async () => {
            const routesTotal = [];
            for (let i = 0; i < routesPromise.length; i++) {
                routesTotal[i] = await routesPromise[i];
            }
            console.log(routesTotal);
            const routes = routesTotal[1];
            var points = [];
            for (let i = 0; i < routesTotal[0].length; i++) {
                points.push(decode(routesTotal[0][i].polyline));
            }
            if (routes.length > 1) {
                for (let i = 0; i < points.length - 1; i++) {
                    var hereLatlng = [points[i + 1].polyline[0][1], points[i + 1].polyline[0][0]];
                    let stage = i + 1;
                    let name = routes[i].arrival.place.brand.name;
                    let time = secondsToHms(routes[i].summary.duration);
                    let lengthWay = Math.round(routes[i].summary.length / 1000);
                    let chargeRemaining = Math.round(routes[i].postActions[0].arrivalCharge / maxCharge * 100);
                    let chargeTarget = Math.round(routes[i].postActions[0].targetCharge / maxCharge * 100);
                    let chargeTime = secondsToHms(routes[i].postActions[0].duration);
                    let herePopup = '<h2>Stage ' + stage + ' hereCar' + '</h2><strong>Charge station name: </strong>' + name + '<br/><strong>Travel time: </strong>' + time + '<br/><strong>Travel length: </strong>' + lengthWay + ' km<br/><strong>Charge: </strong>' + chargeRemaining + '% -> ' + chargeTarget + '%<br/><strong>Charge time: </strong>' + chargeTime + '</br>';
                    popup.push(herePopup);
                    latlng.push(hereLatlng);
                    var marker = new maplibregl.Marker({
                        color: "#0000ff"
                    })
                        .setLngLat(hereLatlng)
                        .addTo(map);
                    var is = false;
                    for (let k = 0; k < latlng.length - 1; k++) {
                        if (latlng[k][0] == hereLatlng[0] && latlng[k][1] == hereLatlng[1]) {
                            let PopupOld = popup[k];
                            marker.setPopup(new maplibregl.Popup({ closeButton: false }).setHTML(PopupOld + herePopup));
                            var is = true;
                        }
                    }

                    if (is == false) {
                        marker.setPopup(new maplibregl.Popup({ closeButton: false }).setHTML(herePopup));
                    }
                    markers.push(marker);
                }
            }
            var lng = routes.length - 1;
            var timeEnd = secondsToHms(routes[lng].summary.duration);
            var lengthWayEnd = routes[lng].summary.length / 1000;
            var totalLength = 0;
            var totalChargingTime = 0;
            var totalTime = 0;
            for (let j = 0; j < routes.length; j++) {
                totalLength += routes[j].summary.length / 1000;
                if (j - 1 >= 0) { totalChargingTime += routes[j - 1].postActions[0].duration; }
                totalTime += routes[j].summary.duration;
            }
            var lastStage = lng + 1;
            var remainingCharge = Math.round(routes[lng].arrival.charge / maxCharge * 100);
            var endPopupHere = '<h2>STAGE ' + lastStage + ' hereCar' + '</h2><strong>Travel time: </strong>' + timeEnd + '<br/><strong>Travel length: </strong>' + lengthWayEnd + ' km<br/><h3>TOTAL ROUTES</h3><strong>Travel time: </strong>' + secondsToHms(totalTime) + '<br/><strong>Charging time: </strong>' + secondsToHms(totalChargingTime) + '<br/><strong>Travel length: </strong>' + Math.round(totalLength) + 'km<br/><strong>Remaining charge: </strong>' + remainingCharge + '%';
            var startPopupHere = '<h3>START hereCar</h3><strong>Max charge: </strong>' + maxCharge + ' kWh</br><strong>Current charge: </strong>' + startCharge/maxCharge*100 + '%</br>';
            endPopup.push(endPopupHere);
            startPopup.push(startPopupHere);
            var is = false;
            let endPopupOld = [];
            let startPopupOld = [];
            for (let k = 0; k < startPopup.length - 1; k++) {
                endPopupOld.push(endPopup[k]);
                startPopupOld.push(startPopup[k]);
                var is = true;
            }

            if (is == false) {
                markerEnd.setPopup(new maplibregl.Popup({ closeButton: false }).setHTML(endPopupHere));
                markerStart.setPopup(new maplibregl.Popup({ closeButton: false }).setHTML(startPopupHere));
            }
            else {
                markerEnd.setPopup(new maplibregl.Popup({ closeButton: false }).setHTML(endPopupOld + endPopupHere));
                markerStart.setPopup(new maplibregl.Popup({ closeButton: false }).setHTML(startPopupOld + startPopupHere));
            }

            const element = [];
            var k = 0;
            for (let j = 0; j < points.length; j++) {
                for (let i = 0; i < points[j].polyline.length; i++) {
                    element[k] = [points[j].polyline[i][1], points[j].polyline[i][0]];
                    k++;
                }
            }
            var idHere = 'routeHere';
            map.addSource(idHere, {
                'type': 'geojson',
                'data': {
                    'type': 'Feature',
                    'properties': {},
                    'geometry': {
                        'type': 'LineString',
                        'coordinates': element
                    }
                }
            });
            map.addLayer({
                'id': idHere,
                'type': 'line',
                'source': idHere,
                'layout': {
                    'line-join': 'round',
                    'line-cap': 'round'
                },
                'paint': {
                    'line-color': '#0000ff',
                    'line-width': 8
                }
            });
            layers.push(idHere);
        }
        printTravelHere();
    }
    //
    //
    //
    //
    //
    function get_geometry_line(geometry_format, route_geometry) {
        if (geometry_format == 'geojson')
            return route_geometry.coordinates

        var latlon_coordinates = polyline.decoder(route_geometry, geometry_format == 'polyline6' ? 6 : 5)
        var lonlat_coordinates = []
        for (var i = 0; i < latlon_coordinates.length; i++)
            lonlat_coordinates.push([latlon_coordinates[i][1], latlon_coordinates[i][0]]);
        return lonlat_coordinates;
    }
    var routesBenomad = async (startCharge, maxCharge, nameB, id) => {
        var lngLat1 = markerStart.getLngLat();
        var lngLat2 = markerEnd.getLngLat();
        var loginPwd = btoa("Mapping_factory_MI:R2TJwKYqsXCez23CjfaI");
        var data = {
            "geoserver": "here",
            "algo": "v3",
            "csps": [
                "hereHlp"
            ],
            "vehicle": nameB,
            "initBatLvl": startCharge / maxCharge * 100,
            "minBatLvl": maxCharge / 10,
            "minArrivalBatLvl": 15,
            "temperature": 20,
            "extraPayload": 75,
            "startLon": lngLat1.lng,
            "startLat": lngLat1.lat,
            "stopLon": lngLat2.lng,
            "stopLat": lngLat2.lat,
            "pl": false,
            "epl": true,
            "cur": "EUR",
            "departureTime": 1657201380000,
            "stepPointPluggingTime": 300
        };
        var settings = {
            "url": " https://bemap-ev-move-preprod.benomad.com/bgis/service/evsmartrouting/1.0",
            "method": "POST",
            "timeout": 0,
            "headers": {
                "Authorization": "Basic " + loginPwd,
                "Content-Type": "application/json"
            },
            "data": JSON.stringify(data)
        };

        var infoBenomad = $.ajax(settings).then((res) => { return res; });
        var printTravelBenomad = async () => {
            var routes = await infoBenomad;
            console.log(routes);
            var carName = " benomad " + routes.journey.vehicle;
            for (let i = 0; i < routes.route.stepPoints.length; i++) {
                var benomadLatlng = [routes.route.stepPoints[i].longitude, routes.route.stepPoints[i].latitude];
                let stage = i + 1;
                let name = routes.route.stepPoints[i].street;
                let time = secondsToHms(routes.route.stepPoints[i].duration);
                let lengthWay = Math.round(routes.route.stepPoints[i].distance / 1000);
                let chargeRemaining = Math.round(routes.route.stepPoints[i].arrivalBatteryLevel);
                let chargeTarget = Math.round(routes.route.stepPoints[i].departureBatteryLevel);
                let chargeTime = secondsToHms(routes.route.stepPoints[i].chargingTime);
                let benomadPopup = '<h2>Stage ' + stage + carName + '</h2><strong>Charge station: </strong>' + name + '<br/><strong>Travel time: </strong>' + time + '<br/><strong>Travel length: </strong>' + lengthWay + ' km<br/><strong>Charge: </strong>' + chargeRemaining + '% -> ' + chargeTarget + '%<br/><strong>Charge time: </strong>' + chargeTime + '</br>';
                popup.push(benomadPopup);
                latlng.push(benomadLatlng);
                var marker = new maplibregl.Marker({
                    color: "#CC00ff"
                })
                    .setLngLat(benomadLatlng)
                    .addTo(map);
                var is = false;
                for (let k = 0; k < latlng.length - 1; k++) {
                    if (latlng[k][0] == benomadLatlng[0] && latlng[k][1] == benomadLatlng[1]) {
                        let PopupOld = popup[k];
                        marker.setPopup(new maplibregl.Popup({ closeButton: false }).setHTML(PopupOld + benomadPopup));
                        var is = true;
                    }
                }

                if (is == false) {
                    marker.setPopup(new maplibregl.Popup({ closeButton: false }).setHTML(benomadPopup));
                }
                markers.push(marker);
            }
            if (routes.route.stepPoints.length > 0) {
                var lng = routes.route.stepPoints.length - 1;
                var timeEnd = secondsToHms(routes.route.stepPoints[lng].duration);
                var lengthWayEnd = routes.route.stepPoints[lng].distance / 1000;
                var lastStage = lng + 1;
            }
            else {
                var lng = 1;
                var timeEnd = secondsToHms(routes.journey.duration);
                var lengthWayEnd = routes.journey.distance/1000;
                var lastStage = lng;
            }
            var totalLength = routes.journey.distance / 1000;
            var totalChargingTime = routes.journey.chargingTime;
            var totalTime = routes.journey.duration;
            var remainingCharge = Math.round(routes.journey.batteryLevel);
            var endPopupBenomad = '<h2>STAGE ' + lastStage + carName + '</h2><strong>Travel time: </strong>' + timeEnd + '<br/><strong>Travel length: </strong>' + lengthWayEnd + ' km<br/><h3>TOTAL ROUTES</h3><strong>Travel time: </strong>' + secondsToHms(totalTime) + '<br/><strong>Charging time: </strong>' + secondsToHms(totalChargingTime) + '<br/><strong>Travel length: </strong>' + Math.round(totalLength) + 'km<br/><strong>Remaining charge: </strong>' + remainingCharge + '%';
            var startPopupBenomad = '<h3>START' + carName + '</h3><strong>Max charge: </strong>' + maxCharge + ' kWh</br><strong>Current charge: </strong>' + startCharge/maxCharge*100 + '%</br>';
            endPopup.push(endPopupBenomad);
            startPopup.push(startPopupBenomad);
            var is = false;
            let endPopupOld = [];
            let startPopupOld = [];
            for (let k = 0; k < startPopup.length - 1; k++) {
                endPopupOld.push(endPopup[k]);
                startPopupOld.push(startPopup[k]);
                var is = true;
            }

            if (is == false) {
                markerEnd.setPopup(new maplibregl.Popup({ closeButton: false }).setHTML(endPopupBenomad));
                markerStart.setPopup(new maplibregl.Popup({ closeButton: false }).setHTML(startPopupBenomad));
            }
            else {
                markerEnd.setPopup(new maplibregl.Popup({ closeButton: false }).setHTML(endPopupOld + endPopupBenomad));
                markerStart.setPopup(new maplibregl.Popup({ closeButton: false }).setHTML(startPopupOld + startPopupBenomad));
            }
            var points = get_geometry_line("polyline5", routes.route.encodedPolyline);
            map.addSource(id, {
                'type': 'geojson',
                'data': {
                    'type': 'Feature',
                    'properties': {},
                    'geometry': {
                        'type': 'LineString',
                        'coordinates': points
                    }
                }
            });
            map.addLayer({
                'id': id,
                'type': 'line',
                'source': id,
                'layout': {
                    'line-join': 'round',
                    'line-cap': 'round'
                },
                'paint': {
                    'line-color': '#CC00ff',
                    'line-width': 8
                }
            });
            layers.push(id);
        }
        printTravelBenomad();
    }


    var start = document.getElementById("button");
    start.addEventListener('click', function () {
        var tomtomzoe = document.getElementById("zoe");
        var tomtombase = document.getElementById("tomtom");
        var herebase = document.getElementById("here");
        var benomadBase = document.getElementById("benomad");
        var benomadBase1 = document.getElementById("benomad1");

        var actualCharge = document.getElementById("currentCharge").value;
        var maximumCharge = document.getElementById("maxCharge").value;
        var consumption = {
            "fifty": document.getElementById("fifty").value,
            "hundred": document.getElementById("hundred").value
        };
        var data = {
            "chargingModes": [
                {
                    "chargingConnections": [
                        {
                            "facilityType": "Charge_380_to_480V_3_Phase_at_32A",
                            "plugType": "IEC_62196_Type_2_Outlet"
                        }
                    ],
                    "chargingCurve": [
                        {
                            "chargeInkWh": maximumCharge * 0.15,
                            "timeToChargeInSeconds": document.getElementById("fifteen").value
                        },
                        {
                            "chargeInkWh": maximumCharge * 0.3,
                            "timeToChargeInSeconds": document.getElementById("thirty").value
                        },
                        {
                            "chargeInkWh": maximumCharge * 0.7,
                            "timeToChargeInSeconds": document.getElementById("seventy").value
                        },
                        {
                            "chargeInkWh": maximumCharge,
                            "timeToChargeInSeconds": document.getElementById("full").value
                        }
                    ]
                },
                {
                    "chargingConnections": [
                        {
                            "facilityType": "Charge_200_to_240V_1_Phase_at_10A",
                            "plugType": "Standard_Household_Country_Specific"
                        }
                    ],
                    "chargingCurve": [
                        {
                            "chargeInkWh": maximumCharge * 0.15,
                            "timeToChargeInSeconds": document.getElementById("fifteenh").value
                        },
                        {
                            "chargeInkWh": maximumCharge * 0.3,
                            "timeToChargeInSeconds": document.getElementById("thirtyh").value
                        },
                        {
                            "chargeInkWh": maximumCharge * 0.7,
                            "timeToChargeInSeconds": document.getElementById("seventyh").value
                        },
                        {
                            "chargeInkWh": maximumCharge,
                            "timeToChargeInSeconds": document.getElementById("fullh").value
                        }
                    ]
                }
            ]
        }
        var id = [0, ' ' + document.getElementById("name").value];

        var maximumCharge2 = document.getElementById("maxCharge2").value;
        var actualCharge2 = document.getElementById("currentCharge2").value;
        var consumption2 = {
            "fifty": document.getElementById("fifty2").value,
            "hundred": document.getElementById("hundred2").value
        };
        var data2 = {
            "chargingModes": [
                {
                    "chargingConnections": [
                        {
                            "facilityType": "Charge_380_to_480V_3_Phase_at_32A",
                            "plugType": "IEC_62196_Type_2_Outlet"
                        }
                    ],
                    "chargingCurve": [
                        {
                            "chargeInkWh": maximumCharge2 * 0.15,
                            "timeToChargeInSeconds": document.getElementById("fifteen2").value
                        },
                        {
                            "chargeInkWh": maximumCharge2 * 0.3,
                            "timeToChargeInSeconds": document.getElementById("thirty2").value
                        },
                        {
                            "chargeInkWh": maximumCharge2 * 0.7,
                            "timeToChargeInSeconds": document.getElementById("seventy2").value
                        },
                        {
                            "chargeInkWh": maximumCharge2,
                            "timeToChargeInSeconds": document.getElementById("full2").value
                        }
                    ]
                },
                {
                    "chargingConnections": [
                        {
                            "facilityType": "Charge_200_to_240V_1_Phase_at_10A",
                            "plugType": "Standard_Household_Country_Specific"
                        }
                    ],
                    "chargingCurve": [
                        {
                            "chargeInkWh": maximumCharge2 * 0.15,
                            "timeToChargeInSeconds": document.getElementById("fifteenh2").value
                        },
                        {
                            "chargeInkWh": maximumCharge2 * 0.3,
                            "timeToChargeInSeconds": document.getElementById("thirtyh2").value
                        },
                        {
                            "chargeInkWh": maximumCharge2 * 0.7,
                            "timeToChargeInSeconds": document.getElementById("seventyh2").value
                        },
                        {
                            "chargeInkWh": maximumCharge2,
                            "timeToChargeInSeconds": document.getElementById("fullh2").value
                        }
                    ]
                }
            ]
        }
        var id2 = [1, ' ' + document.getElementById("name2").value];
        var actualChargeH = document.getElementById("currentChargeH").value;
        var maximumChargeH = document.getElementById("maxChargeH").value;

        var maximumChargeB = document.getElementById("maxChargeB").value;
        var actualChargeB = document.getElementById("currentChargeB").value;
        var carB = document.getElementById("nameB").value;
        var idB = " benomad tesla";

        var maximumChargeB1 = document.getElementById("maxChargeB1").value;
        var actualChargeB1 = document.getElementById("currentChargeB1").value;
        var carB1 = document.getElementById("nameB1").value;
        var idB1 = " benomad zoe";

        var len = layers.length;
        if (len > 0) {
            for (let i = len - 1; i >= 0; i--) {
                map.getSource();
                map.removeLayer(layers[i]);
                map.removeSource(layers[i]);

                layers.pop();
            }
        }
        while (markers.length > 0) {
            markers.pop().remove();
        }
        endPopup = [];
        startPopup = [];
        popup = [];
        latlng = [];

        if (tomtombase.checked) {
            routesTomtom(data, actualCharge, maximumCharge, consumption, color1, id, size1);
        }
        if (tomtomzoe.checked) {
            routesTomtom(data2, actualCharge2, maximumCharge2, consumption2, color2, id2, size2);
        }
        if (herebase.checked) {
            routesHere(actualChargeH, maximumChargeH);
        }
        if (benomadBase.checked) {
            routesBenomad(actualChargeB, maximumChargeB, carB, idB);
        }
        if (benomadBase1.checked) {
            routesBenomad(actualChargeB1, maximumChargeB1, carB1, idB1);
        }
    });
});


